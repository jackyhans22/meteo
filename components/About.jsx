import {View, Text, Image, StyleSheet, Button, ToastAndroid} from "react-native";

export default function About(){
	return(

			<View style={{padding:20}}>
			<Text style={styles.textBold}>
			À propos
			</Text>
			<View style={{fontSize:22, marginTop: 15}}>
			<Text>
			Cett application non définitive montre la flexibilité qu'offre react native tant côté développement que 
			côté interface.
			</Text>
			</View>
			</View>
	)
}

const styles = StyleSheet.create({

	textBold: {

		fontWeight: "Bold",
		fontSize: 30
	}


})