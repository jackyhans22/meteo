import {View, Text, Image, StyleSheet, Button, ToastAndroid} from "react-native";
import Search from "./Search"


export default function Home(){
	return(

			<View style={{padding:20}}>
			 
			<Text style={styles.textBold}>
			Home
			</Text>
			<View style={{fontSize:22, marginTop: 15}}>
			<Text style={{fontSize:22, marginVertical: 15}} >
			This is home
			</Text>
			</View>
				<Search/>
			</View>
	)
}

const styles = StyleSheet.create({

	textBold: {

		fontWeight: "Bold",
		fontSize: 30
	}


})