import { StyleSheet, ActivityIndicator, Text, View, StatusBar} from 'react-native';
import About from "./components/About"
import Search from "./components/Search"
import Home from "./components/Home"
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default function App() {

  const Tab = createBottomTabNavigator();
  return (

    <NavigationContainer>

   {/* <ActivityIndicator size={35} color="#f44" />*/}

      <Tab.Navigator
        screenOptions={({ route} ) => ({
            headerShown: false,
          tabBarIcon: ({ focused, color, size, }) => {
            let iconName;

            if (route.name === 'About') {
              iconName = focused
                ? 'ios-information-circle'
                : 'ios-information-circle-outline';
            } else if (route.name === 'Home') {
              iconName = focused ? 'ios-home' : 'ios-home-outline';
            }

            // You can return any component that you like here!
            return <Ionicons name={iconName} size={size} color={color} />;
          },
          tabBarActiveTintColor: 'tomato',
          tabBarInactiveTintColor: 'gray',
        })}


      >
    
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="About" component={About} />


      </Tab.Navigator>
       <StatusBar hidden={true}/>


    
    </NavigationContainer>
       
  
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
   padding: 20,
  },
});

